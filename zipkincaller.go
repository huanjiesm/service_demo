package main

import (
	"net/http"

	zipkin "github.com/openzipkin/zipkin-go"
	"github.com/openzipkin/zipkin-go/propagation/b3"
)

// ZipkinCaller 函数级的zipkin跟踪器
func ZipkinCaller(r *http.Request, tracer *zipkin.Tracer, name string) {
	// var sc model.SpanContext
	// if parentSpan := zipkin.SpanFromContext(ctx); parentSpan != nil {
	// 	sc = parentSpan.Context()
	// }
	// sp := tracer.StartSpan(name, zipkin.Parent(sc))
	// defer sp.Finish()
	// ctx = zipkin.NewContext(ctx, sp)
	// return ctx, nil

	sc := tracer.Extract(b3.ExtractHTTP(r))
	sp := tracer.StartSpan(name, zipkin.Parent(sc))
	defer sp.Finish()
	ctx := zipkin.NewContext(r.Context(), sp)
	r.WithContext(ctx)
}
