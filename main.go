package main

import (
	"flag"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	stdzipkin "github.com/openzipkin/zipkin-go"
	zipkinhttp "github.com/openzipkin/zipkin-go/middleware/http"
	reporterhttp "github.com/openzipkin/zipkin-go/reporter/http"
	wraphh "github.com/turtlemonvh/gin-wraphh"
)

func main() {

	// 读取参数
	consulAddr := flag.String("consuladdr", "consul.ms.com", "Consul agent address")
	servicePort := flag.Int("port", 3000, "Address Port for HTTP server")
	serviceHost := flag.String("host", "127.0.0.1", "Local Server Address")
	serviceName := flag.String("name", "MAP", "Service Unique Name")
	flag.Parse()

	// 健康检查
	// server := grpc.NewServer()
	// grpc_health_v1.RegisterHealthServer(server, &HealthImpl{})

	// 注册
	reg := NewConsulRegister(*consulAddr, *serviceName, *serviceHost, *servicePort)
	err := reg.Register()
	if err != nil {
		fmt.Println(err)
	}

	// 创建基础服务
	router := gin.Default()

	// 心跳
	router.GET("/health", func(c *gin.Context) {
		c.String(http.StatusOK, "ok")
	})

	// 添加链路跟踪器
	trace := newTracer()
	handler := zipkinhttp.NewServerMiddleware(trace, zipkinhttp.SpanName("bookservice"))
	router.Use(wraphh.WrapHH(handler))

	// client, _ := zipkinhttp.NewClient(trace)

	// 业务请求
	router.GET("/map/x1", func(c *gin.Context) {
		th := c.GetHeader("Test")
		c.Header("Content-Type", "application/json")
		token := c.Query("token")
		buf, _ := c.GetRawData()

		// ZipkinCaller(c.Request, trace, "map/x1")

		c.JSON(http.StatusOK, gin.H{
			"Code":   0,
			"Msg":    "map/x1",
			"Val":    servicePort,
			"Token":  token,
			"Data":   string(buf),
			"Header": th,
		})
	})

	router.Run(":" + strconv.Itoa(*servicePort))

}

// newTracer 创建新的链路跟踪器
func newTracer() *stdzipkin.Tracer {
	zipkinURL := "http://zipkin.ms.com/api/v2/spans"
	zipkinTracer, _ := stdzipkin.NewTracer(reporterhttp.NewReporter(zipkinURL), stdzipkin.WithTraceID128Bit(true))
	return zipkinTracer
}
