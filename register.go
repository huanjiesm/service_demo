package main

import (
	"fmt"
	"net"
	"time"

	"github.com/hashicorp/consul/api"
)

// ConsulRegister 注册对象
type ConsulRegister struct {
	Address                        string
	Service                        string
	LocalIP                        string
	Port                           int
	Tag                            []string
	DeregisterCriticalServiceAfter time.Duration
	Interval                       time.Duration
}

// Register 注册
func (r *ConsulRegister) Register() error {

	config := api.DefaultConfig()
	config.Address = r.Address
	client, err := api.NewClient(config)
	if err != nil {
		return err
	}
	agent := client.Agent()
	localip := r.LocalIP
	if localip == "" {
		localip = localIP()
	}

	reg := &api.AgentServiceRegistration{
		ID:   fmt.Sprintf("%v-%v-%v", r.Service, localip, r.Port),
		Name: fmt.Sprintf(r.Service),
		Tags: r.Tag,
		Port: r.Port,
		Check: &api.AgentServiceCheck{
			Interval: r.Interval.String(),
			// GRPC:     fmt.Sprintf("%v:%v/%v", IP, r.Port, r.Service),
			HTTP:                           fmt.Sprintf("http://%s:%d%s", localip, r.Port, "/health"),
			DeregisterCriticalServiceAfter: r.DeregisterCriticalServiceAfter.String(),
		},
	}
	err = agent.ServiceRegister(reg)
	if err != nil {
		return err
	}
	return nil
}

// NewConsulRegister 创建新注册对象
func NewConsulRegister(address string, servicename string, servicehost string, serviceport int) *ConsulRegister {
	return &ConsulRegister{
		Address:                        address,
		Service:                        servicename,
		Tag:                            []string{"BookService"},
		LocalIP:                        servicehost,
		Port:                           serviceport,
		DeregisterCriticalServiceAfter: time.Duration(1) * time.Minute,
		Interval:                       time.Duration(10) * time.Second,
	}
}

// localIP 获取本地IP
func localIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		ipnet, ok := address.(*net.IPNet)
		if ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}
